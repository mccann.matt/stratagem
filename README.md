Strategem: Chromo Commander

This darling little table-top-esque strategy game was a passion project pursued by myself and two friends with whom I was living at the time. I wrote the code to drive the game with my more artistic friends providing animations and in-game music. We built a fully functional demo that allowed two players to select their army of minions, place their pieces on the field, and play through the game to completion. It turned out to be quite challenging to build the game, as I opted to build all of it from scratch, including the game engine itself.
